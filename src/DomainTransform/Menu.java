package DomainTransform;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
 
public class Menu extends javax.swing.JFrame {
	private static final long serialVersionUID = 1L;
	
		@Override
		public void setTitle(String arg0) {
			// TODO Auto-generated method stub
			super.setTitle("OPERATIONS");
		}
	 	private javax.swing.JButton Load;
	    private javax.swing.JButton Filter;
	    private javax.swing.JButton Save;
	    private javax.swing.JButton Restart;
	    private javax.swing.JButton Quit;
        private javax.swing.JTextField SigmaSpace;
        private javax.swing.JTextField SigmaRange;
        private javax.swing.JTextArea jtaArea;        
        
		BufferedImage image = null;
		BufferedImage imageCopy = null;
		File arquivo = null;
		
		ImageIcon imageIcon = null;
		ImageIcon imageIconCopy = null;
        JLabel jLabel = null;
        JLabel jLabelCopy = null;
        JPanel subPanel = new JPanel();
        JFrame copyFrame = new JFrame("Copy image");
        JFrame originalFrame = new JFrame("Original image");
        
        public Menu() {
	        initComponents();
	    }
	     
	    private void initComponents() { 
	        Load 	  	     = new javax.swing.JButton();
	        Filter 	   	     = new javax.swing.JButton();
	        Save 	   	     = new javax.swing.JButton();
	        Restart	   	     = new javax.swing.JButton();
	        Quit             = new javax.swing.JButton();
	        SigmaSpace       = new javax.swing.JTextField(5);
	        SigmaRange       = new javax.swing.JTextField(5);
	        
	        Container cont1 = getContentPane();
	        cont1.setLayout(new javax.swing.BoxLayout(cont1, javax.swing.BoxLayout.X_AXIS));
	        cont1.setLocation(1050, 50);
	        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
	        
	        Panel panel = new Panel();
	        panel.setLayout(new javax.swing.BoxLayout(panel,javax.swing.BoxLayout.Y_AXIS));

	        cont1.add(panel);
	        
	        Load.setText("LOAD IMAGE");
	        Load.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                botaoLoadActionPerformed(evt);
	            }
	        });
	 
	        panel.add(Load);
	        
	        Filter.setText("APPLY FILTER");
	        Filter.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                botaoFilterActionPerformed(evt);
	            }
	        });
	 
	        panel.add(Filter);
	        
	        Restart.setText("RESTART");
	        Restart.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	            	botaoRestartActionPerformed(evt);
	            }
	        });
	 
	        panel.add(Restart);
	 
	        Save.setText("SAVE THE COPY");
	        Save.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                botaoCopyActionPerformed(evt);
	            }
	        });
	 
	        panel.add(Save);
	 
	        SigmaSpace.setText("Sigma Space");
	        panel.add(SigmaSpace);
	        
	        SigmaRange.setText("Sigma Range");
	        panel.add(SigmaRange);
	       	        
	        Quit.setText("QUIT");
	        Quit.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                botaoQuitActionPerformed(evt);
	            }
	        });
	        	        
	        panel.add(Quit);
	 
	        pack();
	    }
	 
	    
	    private void botaoCopyActionPerformed(java.awt.event.ActionEvent evt) {  
		    //read image file
		    if((imageCopy != null) && (arquivo != null)) {
		    	String filepath = arquivo.getPath();
		    	String filename = arquivo.getName();
		    	filepath = filepath.substring(0, filepath.lastIndexOf('\\'));
		    	filename = "\\CopyOf" +filename;	
		    	filepath = filepath+filename;
		    	File file = null;
				
				try{
					file = new File(filepath); //outputfile
					ImageIO.write(imageCopy, "jpg", file);
					JOptionPane.showMessageDialog(Restart, "Imagem salva com sucesso!");
					
				} catch (IOException e){
					System.out.println("Error: "+e);
				}
				
		    } else
		      JOptionPane.showMessageDialog(Restart,
		    	        "Error: Carregue uma imagem primeiro!", 
		    	        "Erro", 
		    	        JOptionPane.INFORMATION_MESSAGE);
		    }
	    
	    private void botaoRestartActionPerformed(java.awt.event.ActionEvent evt) {  
		    //read image file
		    if((imageCopy != null) && (arquivo != null)) {
		    	imageCopy = image;
		    	imageIconCopy = imageIcon;
		    	copyFrame = originalFrame;
		    	jLabelCopy = jLabel;		    	
				
		    } else
		      JOptionPane.showMessageDialog(Restart,
		    	        "Error: Carregue uma imagem primeiro!", 
		    	        "Erro", 
		    	        JOptionPane.INFORMATION_MESSAGE);
		    }
	 
	    private void botaoLoadActionPerformed(java.awt.event.ActionEvent evt) {     
	    	originalFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
	    	copyFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
	    	
	    	BufferedImage newImage = null;
		    try {
	            JFileChooser jfc = new JFileChooser();
	            FileNameExtensionFilter filter = new FileNameExtensionFilter("Formatos de Archivo JPEG(*.JPG;*.JPEG)","jpg","jpeg");
	    		jfc.addChoosableFileFilter(filter);
	    		jfc.setFileFilter(filter);
	            jfc.setApproveButtonText("Selecionar arquivo");
	            jfc.setDialogTitle("Buscar arquivo");
	            jfc.setAcceptAllFileFilterUsed(false);
	            jtaArea = new javax.swing.JTextArea();	            
	            int returnVal = jfc.showOpenDialog(this);
	            
	            if (!(returnVal == JFileChooser.APPROVE_OPTION)) {
	                return;
	            }
	            
	            arquivo = jfc.getSelectedFile();
	            jtaArea.setText("NAME FILE: " + arquivo.getName() + "\n"
	                    + "PATH: " + arquivo.getPath() + "\n"
	                    + "PARENT: " + arquivo.getParent() + "\n"
	                    + "CANONICAL PATH: " + arquivo.getCanonicalPath() + "\n"
	                    + "ABSOLUTE PATH: " + arquivo.getAbsolutePath() + "\n"
	            );
	            
	            newImage = ImageIO.read(arquivo);	
	            	
		    } catch(IOException ef) {
				JOptionPane.showMessageDialog(Load, "Não foi possível carregar a imagem. Diretório errado ou imagem não existe.");
			}
		    if (newImage != null) {		    	
		    	if (image == null) {
		    		image = newImage;
		    		imageIcon = new ImageIcon(image);
		    		jLabel = new JLabel();
		    		jLabel.setIcon(imageIcon);
		    		originalFrame.getContentPane().add(jLabel, BorderLayout.CENTER);
		    		originalFrame.pack();
		    		originalFrame.setLocation(400, 10);
		    		originalFrame.setVisible(true);
		    	}
		    	else {
		    		image = newImage;
	    			imageIcon = new ImageIcon(image);
	    			jLabel.setIcon(imageIcon);
		    	}
		    		
		    	if (imageCopy == null) {
			    	imageCopy = newImage;
		    		imageIconCopy = new ImageIcon(imageCopy);
			        jLabelCopy = new JLabel();
			        jLabelCopy.setIcon(imageIconCopy);
			        
			        subPanel.add(jLabelCopy);
			        
			        copyFrame.getContentPane().add(subPanel, BorderLayout.CENTER);
			        copyFrame.pack();
			        copyFrame.setLocation(950, 10);
			        copyFrame.setVisible(true);
			        
			        
			        
		    	}
		    	else {
		    		imageCopy = newImage;
	    			imageIconCopy = new ImageIcon(imageCopy);
	    			jLabelCopy.setIcon(imageIconCopy);
		    	}
	        			        
		    }
	    }                                      
	    
	    private void botaoFilterActionPerformed(java.awt.event.ActionEvent evt) {
	    	if((imageCopy != null) && (arquivo != null)) {
		    			    	
				
		    } else
		      JOptionPane.showMessageDialog(Filter,
		    	        "Error: Carregue uma imagem primeiro!", 
		    	        "Erro", 
		    	        JOptionPane.INFORMATION_MESSAGE);
		    }
	    	
	   
	    private void botaoQuitActionPerformed(java.awt.event.ActionEvent evt) {                                       
	    	System.exit(0);
	    } 
	    
	    public static void main(String args[]) {
	        java.awt.EventQueue.invokeLater(new Runnable() {
	            public void run() {
	                new Menu().setVisible(true);
	            }
	        });
	    }
}
	     